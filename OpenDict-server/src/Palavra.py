import json

################ PALAVRA-CLASS #####################
class Palavra:
    def __init__(self, vocabulo, classes, significados):
        self.vocabulo = vocabulo
        self.classes = classes
        self.significados = significados
                
    def encode(self):
        return json.dumps(   self, 
                            default=lambda o: o.__dict__, 
                            sort_keys=True, 
                            indent=4    ).encode()
    
    def decode(dct):
        return Palavra(dct["vocabulo"],dct["classes"], dct["significados"])
    
    def write(array):
        with open('all_words.json', 'w') as file:
            return json.dump(   array,
                                file,
                                default=lambda o: o.__dict__, 
                                sort_keys=True, 
                                indent=4    )
    
    def get_all():
        with open('all_words.json', 'r') as file:
            data = json.load(file, object_hook=Palavra.decode)
            return data
        

