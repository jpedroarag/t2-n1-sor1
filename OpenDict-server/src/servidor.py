################ SERVIDOR ########################
import socket
import datetime
from Palavra import *

def checar_SO():
    import sys
    plataformas = {
        'linux1' : 'Linux',
        'linux2' : 'Linux',
        'darwin' : 'OS X',
        'win32'  : 'Windows'
    }
    plataforma = sys.platform
    if plataforma not in plataformas:
        return plataforma
    
    return plataformas[plataforma]


def get_HOST(so):
    if so == "Windows":
        return socket.gethostname()
    else:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        return s.getsockname()[0]
    
    
SO = checar_SO()
HOST = get_HOST(SO)
PORT = 12233
MAX_BYTES = 65000


palavras = Palavra.get_all()

def contains(array, elemento):
    for element in array:
        if elemento == element.vocabulo:
            return element
    return None


def verificar_operacao(texto):
    verificacao = texto.split()[0]
    
    if verificacao == '1':
        texto = texto.replace('1 ','')
        return consultar(texto).encode()
    elif verificacao == '2':
        texto = texto.replace('2 ','')
        return inserir(texto).encode()
    else:
        return 'Operação inválida'.encode()


def consultar(palavra):
    word = contains(palavras, palavra.lower())
    if word != None:
        return word
    else:
        return 'Erro: Não encontrado!'
    
def inserir(palavra):
    import json
    palavra_json = json.loads(palavra)
    if contains(palavras, palavra_json["vocabulo"].lower()) == None:
        new_palavra = Palavra.decode(palavra_json)
        new_palavra.vocabulo = new_palavra.vocabulo.lower()
        palavras.append(new_palavra)
        Palavra.write(palavras)
        return 'Inserido com sucesso!'.format(palavra)
    else:
        return 'Palavra já existente no servidor...'


if __name__ == '__main__':
    print('Servidor ({}) inicializado em {}:{} na data {}'.format(SO, HOST, PORT, datetime.datetime.now()))
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind((HOST, PORT))
#    r = requests.get("http://dicionario-aberto.net/search-json/manga")

    while True:
        print("------------------------------------")
        print('Esperando por conexão UDP ...')
        data, address = sock.recvfrom(MAX_BYTES)
        print('Conexão iniciada na data {}'.format(datetime.datetime.now()))
        text = data.decode()
        print('Recebido {!r} do endereço {}'.format(text, address))
        response = verificar_operacao(text)
        print('Enviando resposta ...')
        sock.sendto(response, address)
        print("------------------------------------")
