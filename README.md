﻿# T2-N1-SOR1
Implementar um dicionário online colaborativo. Utilizar sockets UDP
para realizar as consultas entre um cliente e um servidor.

O servidor deve conter pelo menos 100 termos para consulta
do usuário. Caso um termo não esteja disponível uma
mensagem deve ser exibida ao usuário. O servidor deve
disponibilizar duas funções básicas: "consultar" e "incluir" termos.

O cliente deve apresentar ao usuário informações básicas
sobre os comandos disponíveis, nome da aplicação, opção de
saída, etc. Comunicar com o servidor utilizando UDP.