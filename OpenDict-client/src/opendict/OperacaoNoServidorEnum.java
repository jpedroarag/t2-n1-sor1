package opendict;

public enum OperacaoNoServidorEnum {
    consulta(1), insercao(2);
    
    private final int value;
    
    OperacaoNoServidorEnum(int valor) {
        this.value = valor;
    }
    
    public int rawValue() {
        return this.value;
    }
}
