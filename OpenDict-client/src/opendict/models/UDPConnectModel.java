package opendict.models;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import opendict.Alert;
import opendict.OperacaoNoServidorEnum;

public class UDPConnectModel {
    
    public static final int MAX_BYTES = 65000;
    public static String IP = "127.0.0.1";
    public static int porta = 12233;

    public String send(String texto, OperacaoNoServidorEnum operacao) throws SocketException,
                                                                                UnknownHostException,
                                                                                IOException {
        
        BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));

        // Socket UDP
        DatagramSocket clientSocket = new DatagramSocket();

        // IP
        InetAddress IPAddress = InetAddress.getByName(UDPConnectModel.IP);

        // Dados a enviar e a receber
        byte[] sendData = new byte[UDPConnectModel.MAX_BYTES];
        byte[] receiveData = new byte[UDPConnectModel.MAX_BYTES];
        
        // Enviando ao servidor
        texto = operacao.rawValue() + " " + texto;
        sendData = texto.getBytes();
        
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, UDPConnectModel.porta);
        clientSocket.send(sendPacket);
        
        // Recebendo do servidor
        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
        clientSocket.receive(receivePacket);
        
        // Tratando resposta do servidor
        String modifiedSentence = new String(receivePacket.getData());
        clientSocket.close();
        
        return modifiedSentence;
    }
    
}
