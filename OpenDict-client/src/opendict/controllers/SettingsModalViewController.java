/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package opendict.controllers;

import java.net.InetAddress;
import java.net.UnknownHostException;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import opendict.Alert;
import opendict.models.UDPConnectModel;

/**
 *
 * @author jpedr
 */
public class SettingsModalViewController extends Alert {
    
    public SettingsModalViewController(String title) {
        super(title, "", AlertType.OK_CANCEL);
    }
    
    public void present() throws UnknownHostException {
        
        Label ipLabel = new Label("IP");
        TextField ipTextField = new TextField(UDPConnectModel.IP);
        ipTextField.setPromptText("Digite o IP do servidor");
        
        Label portaLabel = new Label("Porta");
        TextField portaTextField = new TextField(""+UDPConnectModel.porta);
        portaTextField.setPromptText("Digite a porta do servidor");
        
        VBox vert = new VBox(8);
        vert.getChildren().addAll(ipLabel, ipTextField, portaLabel, portaTextField);
        
        super.present(true, vert);
        
        if(this.flagResult) {
            if (!ipTextField.getText().isEmpty()
                && !portaTextField.getText().isEmpty()) {
                UDPConnectModel.IP = ipTextField.getText();
                UDPConnectModel.porta = Integer.parseInt(portaTextField.getText());
            } else if(ipTextField.getText().isEmpty()
                && !portaTextField.getText().isEmpty()) {
                UDPConnectModel.IP =  InetAddress.getLocalHost().getHostAddress();
                UDPConnectModel.porta = Integer.parseInt(portaTextField.getText());
            } else if(!ipTextField.getText().isEmpty()
                && portaTextField.getText().isEmpty()) {
                UDPConnectModel.IP = ipTextField.getText();
                UDPConnectModel.porta = 12233;
            } else {
                UDPConnectModel.IP = ipTextField.getText();
                UDPConnectModel.porta = 12233;
            }
        }
    }
    
}
