package opendict.controllers;

import opendict.Alert;
import java.io.IOException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ResourceBundle;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Screen;
import javafx.util.Duration;
import opendict.Alert.AlertType;
import opendict.OperacaoNoServidorEnum;
import opendict.models.UDPConnectModel;
import org.json.JSONArray;
import org.json.JSONObject;

public class ConsultarPalavraViewController implements Initializable {

    @FXML
    Button voltarButton;

    @FXML
    Button consultarButton;

    @FXML
    TextField palavraTextField;

    @FXML
    AnchorPane anchorRoot;
    
    @FXML
    Label vocabuloLabel;

    @FXML
    TreeView<Label> resultadoTreeView;

    OperacaoNoServidorEnum operacao = OperacaoNoServidorEnum.consulta;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        resultadoTreeView.setPrefHeight(bounds.getHeight() - 95);
        vocabuloLabel.setText("");
    }

    @FXML
    private void voltarButtonAction(ActionEvent e) throws IOException {
        URL url = getClass().getResource("views/FXMLDocument.fxml");
        Parent root = FXMLLoader.load(url);

        Scene scene = consultarButton.getScene();
        root.translateXProperty().set(-scene.getWidth());

        StackPane parentContainer = (StackPane) voltarButton.getScene().getRoot();
        parentContainer.getChildren().add(root);

        Timeline timeline = new Timeline();
        KeyValue kv = new KeyValue(root.translateXProperty(), 0, Interpolator.LINEAR);
        KeyFrame kf = new KeyFrame(Duration.seconds(0.5), kv);
        timeline.getKeyFrames().add(kf);

        timeline.setOnFinished(t -> {
            parentContainer.getChildren().remove(anchorRoot);
        });
        timeline.play();
    }

    @FXML
    private void consultarButtonAction(ActionEvent e) throws UnknownHostException, IOException {
        if (!palavraTextField.getText().isEmpty()) {
            UDPConnectModel UDPModel = new UDPConnectModel();
            String palavraString = UDPModel.send(palavraTextField.getText(), operacao);

            if (palavraString.equalsIgnoreCase("Erro: Não encontrado!")) {
                new Alert("Ops...", palavraString, AlertType.OK).present();
            } else {
                JSONObject palavraJSON = new JSONObject(palavraString);
                setTreeViewItens(palavraJSON);
            }
        }
    }

    private void setTreeViewItens(JSONObject palavraJSON) {
        String vocabulo = palavraJSON.getString("vocabulo");
        String initialChar = "" + vocabulo.charAt(0);
        vocabulo = vocabulo.replaceFirst(initialChar, initialChar.toUpperCase());

        vocabuloLabel.setText(vocabulo);
        vocabuloLabel.getStyleClass().add("tree-cell-title");
        
        Label rootLabel = new Label(vocabulo);
        rootLabel.getStyleClass().removeAll("label");
        rootLabel.getStyleClass().add("tree-cell-sub");
        
        TreeItem<Label> root = new TreeItem<>(rootLabel);
        root.setExpanded(true);
        
        resultadoTreeView.setRoot(root);
        resultadoTreeView.setShowRoot(false);

        Label classesLabel = new Label("Classes Gramaticais");
        classesLabel.getStyleClass().removeAll("label");
        classesLabel.getStyleClass().add("tree-cell");
        
        TreeItem<Label> classesItem = new TreeItem<>(classesLabel);
        classesItem.setExpanded(true);
        root.getChildren().add(classesItem);
        
        JSONArray classes = palavraJSON.getJSONArray("classes");
        for (Object obj : classes) {
            String current = (String) obj;
            addTreeItem(current, classesItem);
        }

        Label significadosLabel = new Label("Significados");
        significadosLabel.getStyleClass().removeAll("label");
        significadosLabel.getStyleClass().add("tree-cell");
        
        TreeItem<Label> significadosItem = new TreeItem<>(significadosLabel);
        significadosItem.setExpanded(true);
        root.getChildren().add(significadosItem);
        
        JSONArray significados = palavraJSON.getJSONArray("significados");
        for (int i = 0; i < significados.length(); i++) {
            String current = (String) significados.get(i);
            addTreeItem((i + 1) + " - " + current, significadosItem);
        }

    }

    private TreeItem<Label> addTreeItem(String item, TreeItem<Label> parent) {
        Label subLabel = new Label(item);
        subLabel.getStyleClass().removeAll("label", "tree-view-cell");
        subLabel.getStyleClass().add("tree-view-cell-sub");
        
        TreeItem<Label> treeItem = new TreeItem<>(subLabel);
        treeItem.setExpanded(true);
        
        parent.getChildren().add(treeItem);
        return treeItem;
    }

}
