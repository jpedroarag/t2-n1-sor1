package opendict.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import javafx.util.Duration;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import opendict.Alert;
import opendict.models.UDPConnectModel;

public class FXMLDocumentController implements Initializable {

    @FXML
    Button inserirButton;

    @FXML
    Button consultarButton;
    
    @FXML
    Button settingsButton;

    @FXML
    AnchorPane anchorRoot;

    @FXML
    StackPane parentContainer;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }
    
    private void performSegue(Parent root) {
        Scene scene = consultarButton.getScene();
        root.translateXProperty().set(scene.getWidth());

        parentContainer.getChildren().add(root);

        Timeline timeline = new Timeline();
        KeyValue kv = new KeyValue(root.translateXProperty(), 0, Interpolator.LINEAR);
        KeyFrame kf = new KeyFrame(Duration.seconds(0.5), kv);
        timeline.getKeyFrames().add(kf);
        
        timeline.setOnFinished(t -> {
            parentContainer.getChildren().remove(anchorRoot);
        });
        timeline.play();
    }

    @FXML
    private void inserirButtonAction(ActionEvent e) throws IOException {
        URL url = getClass().getResource("views/InserirPalavraView.fxml");
        Parent root = FXMLLoader.load(url);
        this.performSegue(root);
    }

    @FXML
    private void consultarButtonAction(ActionEvent e) throws IOException {
        URL url = getClass().getResource("views/ConsultarPalavraView.fxml");
        Parent root = FXMLLoader.load(url);
        this.performSegue(root);
    }
    
    @FXML
    private void settingsButtonAction(ActionEvent e) {
        SettingsModalViewController settingsModal = new SettingsModalViewController("Configurações do servidor");
        settingsModal.present();
    }

}
