/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package opendict.controllers;

import java.io.IOException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.util.Duration;
import opendict.Alert;
import opendict.OperacaoNoServidorEnum;
import opendict.models.UDPConnectModel;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author jpedr
 */
public class InserirPalavraViewController implements Initializable {

    @FXML
    TextField palavraTextField;

    @FXML
    Button voltarButton;

    @FXML
    TextField classesTextField;

    @FXML
    Button classesPlusButton;

    @FXML
    Button classesMinusButton;

    @FXML
    VBox classesVBox;

    @FXML
    TextField significadosTextField;

    @FXML
    Button significadosPlusButton;

    @FXML
    Button significadosMinusButton;

    @FXML
    VBox significadosVBox;

    @FXML
    Button concluidoButton;

    @FXML
    AnchorPane anchorRoot;
    
    @FXML
    ScrollPane fieldsScrollPane;

    OperacaoNoServidorEnum operacao = OperacaoNoServidorEnum.insercao;

    private ArrayList<TextField> allClassesTextFields = new ArrayList<>();
    private ArrayList<Button> allClassesPlusButtons = new ArrayList<>();
    private ArrayList<Button> allClassesMinusButtons = new ArrayList<>();

    private ArrayList<TextField> allSignificadosTextFields = new ArrayList<>();
    private ArrayList<Button> allSignificadosPlusButtons = new ArrayList<>();
    private ArrayList<Button> allSignificadosMinusButtons = new ArrayList<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        allClassesPlusButtons.add(classesPlusButton);
        allClassesMinusButtons.add(classesMinusButton);
        allClassesTextFields.add(classesTextField);

        allSignificadosPlusButtons.add(significadosPlusButton);
        allSignificadosMinusButtons.add(significadosMinusButton);
        allSignificadosTextFields.add(significadosTextField);
        
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        fieldsScrollPane.setPrefHeight(bounds.getHeight());
    }

    @FXML
    private void voltarButtonAction(ActionEvent e) throws IOException {
        URL url = getClass().getResource("views/FXMLDocument.fxml");
        Parent root = FXMLLoader.load(url);

        Scene scene = voltarButton.getScene();
        root.translateXProperty().set(-scene.getWidth());

        StackPane parentContainer = (StackPane) voltarButton.getScene().getRoot();
        parentContainer.getChildren().add(root);

        Timeline timeline = new Timeline();
        KeyValue kv = new KeyValue(root.translateXProperty(), 0, Interpolator.LINEAR);
        KeyFrame kf = new KeyFrame(Duration.seconds(0.5), kv);
        timeline.getKeyFrames().add(kf);

        timeline.setOnFinished(t -> {
            parentContainer.getChildren().remove(anchorRoot);
        });
        timeline.play();
    }

    @FXML
    private void classesPlusButtonAction(ActionEvent e) {
        HBox novaClasse = new HBox(10);
        novaClasse.setPrefHeight(40);

        TextField novaClasseTextField = new TextField();
        novaClasseTextField.setPrefWidth(285);
        allClassesTextFields.add(novaClasseTextField);

        Button novaClassePlusButton = new Button("+");
        novaClassePlusButton.setOnAction(ev -> {
            this.classesPlusButtonAction(ev);
        });
        novaClassePlusButton.setStyle("-fx-background-radius: 3em;");
        novaClassePlusButton.setPrefSize(37, 37);
        allClassesPlusButtons.add(novaClassePlusButton);

        Button novaClasseMinusButton = new Button("-");
        novaClasseMinusButton.setOnAction(ev -> {
            this.classesMinusButtonAction(ev);
        });
        novaClasseMinusButton.setStyle("-fx-background-radius: 3em;");
        novaClasseMinusButton.setPrefSize(37, 37);
        allClassesMinusButtons.add(novaClasseMinusButton);

        novaClasse.getChildren().addAll(novaClasseTextField, novaClassePlusButton, novaClasseMinusButton);
        classesVBox.getChildren().add(novaClasse);
    }

    @FXML
    private void classesMinusButtonAction(ActionEvent e) {
        for (Button button : allClassesMinusButtons) {
            if (button == (Button) e.getSource() && allClassesMinusButtons.size() > 1) {
                int index = allClassesMinusButtons.indexOf(button);

                allClassesPlusButtons.remove(index);
                allClassesMinusButtons.remove(index);
                allClassesTextFields.remove(index);
                classesVBox.getChildren().remove(index);
            }
        }
    }

    @FXML
    private void significadosPlusButtonAction(ActionEvent e) {
        HBox novoSignificado = new HBox(10);
        novoSignificado.setPrefHeight(40);

        TextField novoSignificadoTextField = new TextField();
        novoSignificadoTextField.setPrefWidth(285);
        allSignificadosTextFields.add(novoSignificadoTextField);

        Button novoSignificadoPlusButton = new Button("+");
        novoSignificadoPlusButton.setOnAction(ev -> {
            this.significadosPlusButtonAction(ev);
        });
        novoSignificadoPlusButton.setStyle("-fx-background-radius: 3em;");
        novoSignificadoPlusButton.setPrefSize(37, 37);
        allSignificadosPlusButtons.add(novoSignificadoPlusButton);

        Button novoSignificadoMinusButton = new Button("-");
        novoSignificadoMinusButton.setOnAction(ev -> {
            this.significadosMinusButtonAction(ev);
        });
        novoSignificadoMinusButton.setStyle("-fx-background-radius: 3em;");
        novoSignificadoMinusButton.setPrefSize(37, 37);
        allSignificadosMinusButtons.add(novoSignificadoMinusButton);

        novoSignificado.getChildren().addAll(novoSignificadoTextField,
                novoSignificadoPlusButton,
                novoSignificadoMinusButton);
        significadosVBox.getChildren().add(novoSignificado);
    }

    @FXML
    private void significadosMinusButtonAction(ActionEvent e) {
        for (Button button : allSignificadosMinusButtons) {
            if (button == (Button) e.getSource() && allSignificadosMinusButtons.size() > 1) {
                int index = allSignificadosMinusButtons.indexOf(button);

                significadosVBox.getChildren().remove(index);
                allSignificadosPlusButtons.remove(index);
                allSignificadosMinusButtons.remove(index);
                allSignificadosTextFields.remove(index);

            }
        }
    }

    @FXML
    private void concluidoButtonAction(ActionEvent e) throws UnknownHostException, IOException {
        if (palavraTextField.getText().isEmpty()) {
            new Alert("Erro", "Por favor, preencha todos os campos!", Alert.AlertType.OK).present();
        } else {
            String palavra = palavraTextField.getText();
            
            // JSON support by -> https://github.com/stleary/JSON-java
            JSONObject palavraJSON = new JSONObject();
            JSONArray classesJSON = new JSONArray();
            JSONArray significadosJSON = new JSONArray();

            for (TextField tf : allClassesTextFields) {
                if (!tf.getText().isEmpty()) {
                    classesJSON.put(tf.getText());
                }
            }

            for (TextField tf : allSignificadosTextFields) {
                if (!tf.getText().isEmpty()) {
                    significadosJSON.put(tf.getText());
                }
            }

            palavraJSON.put("vocabulo", palavra);
            palavraJSON.put("classes", classesJSON);
            palavraJSON.put("significados", significadosJSON);

            UDPConnectModel UDPModel = new UDPConnectModel();
            String response = UDPModel.send(palavraJSON.toString(), operacao);
            new Alert("Resposta do servidor", response, Alert.AlertType.OK).present();
        }
    }

}
