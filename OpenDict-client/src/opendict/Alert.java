package opendict;

import java.util.ArrayList;
import javafx.beans.InvalidationListener;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Classe com três tipos de Alerts
 *
 * @author João Pedro
 */
public class Alert {

    protected Stage window = new Stage();
    protected final ArrayList<Button> buttons = new ArrayList<>();
    protected Boolean flagResult = null;

    public String title;
    public StringProperty message;
    public AlertType type;

    public enum AlertType {
        OK(0), YES_NO(1), OK_CANCEL(2), TIMEOUT(3);
        private final int value;

        AlertType(int option) {
            value = option;
        }
    }

    public Alert(String title, String message, AlertType type) {
        this.title = title;
        this.message = new SimpleStringProperty(message);
        this.type = type;
    }
    
    public Boolean present(Node... aditionalElements) {
        return this.present(false);
    }

    public Boolean present(boolean omitMessageLabel, Node... aditionalElements) {

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(this.title);

        switch (type) {
            case OK:
                createOkAlert(window);
                break;
            case YES_NO:
                createYesNoAlert(window);
                break;
            case OK_CANCEL:
                createOkCancelAlert(window);
                break;
            case TIMEOUT:
                break;
            default:
                createOkAlert(window);

        }

        // label
        Label label = new Label(this.message.getValue());
        label.textProperty().bind(this.message);

        // layout e botoes
        VBox verticalLayout = new VBox(10);
        if (!omitMessageLabel) { verticalLayout.getChildren().add(label); }
        verticalLayout.getChildren().addAll(aditionalElements);

        if (type != AlertType.TIMEOUT) {
            HBox buttonsLayout = new HBox();
            for(Button btn : buttons) { buttonsLayout.getChildren().add(btn); }
            buttonsLayout.setSpacing(8);
            buttonsLayout.setAlignment(Pos.CENTER);
            verticalLayout.getChildren().add(buttonsLayout);
        }

        verticalLayout.setAlignment(Pos.CENTER);
        verticalLayout.setPadding(new Insets(10, 10, 10, 10));
        AnchorPane.setTopAnchor(verticalLayout, 0.0);
        AnchorPane.setRightAnchor(verticalLayout, 0.0);
        AnchorPane.setBottomAnchor(verticalLayout, 0.0);
        AnchorPane.setLeftAnchor(verticalLayout, 0.0);

        AnchorPane root = new AnchorPane();
        root.getChildren().add(verticalLayout);
        root.setStyle("-fx-background-color: #EEEEEE;");

        // cena
        Scene presentedScene = new Scene(root);

        window.setScene(presentedScene);

        // show
        window.setResizable(false);
        if(type == AlertType.TIMEOUT) {
            window.show();
        } else {
            window.showAndWait();
        }
        return flagResult;
    }

    public void dismiss() {
        window.close();
    }

    private void createOkAlert(Stage window) {
        Button button = new Button("OK");
        button.setOnAction(e -> {
            flagResult = null;
            window.close();
        });
        button.setStyle("-fx-background-color: #303030;"
                    + " -fx-text-fill: #FCF5F5;"
                    + " -fx-font-weight: bold;"
                    + " -fx-cursor: 'hand';");

        buttons.add(button);
    }

    private void createYesNoAlert(Stage window) {
        Button buttonYes = new Button("Yes");
        buttonYes.setOnAction(e -> {
            flagResult = true;
            window.close();
        });
        buttonYes.setStyle("-fx-background-color: #15B515;"
                            + " -fx-text-fill: #FCF5F5;"
                            + " -fx-font-weight: bold;"
                            + " -fx-cursor: 'hand';");

        Button buttonNo = new Button("No");
        buttonNo.setOnAction(e -> {
            flagResult = false;
            window.close();
        });
        buttonNo.setStyle("-fx-background-color: #CA1515;"
                            + " -fx-text-fill: #FCF5F5;"
                            + " -fx-font-weight: bold;"
                            + " -fx-cursor: 'hand';");

        buttons.add(buttonYes);
        buttons.add(buttonNo);
    }

    private void createOkCancelAlert(Stage window) {
        Button buttonOk = new Button("OK");
        buttonOk.setOnAction(e -> {
            flagResult = true;
            window.close();
        });
        buttonOk.setStyle("-fx-background-color: #15B515;"
                            + " -fx-text-fill: #FCF5F5;"
                            + " -fx-font-weight: bold;"
                            + " -fx-cursor: 'hand';");

        Button buttonCancel = new Button("Cancel");
        buttonCancel.setOnAction(e -> {
            flagResult = false;
            window.close();
        });
        buttonCancel.setStyle("-fx-background-color: #CA1515;"
                        + " -fx-text-fill: #FCF5F5;"
                        + " -fx-font-weight: bold;"
                        + " -fx-cursor: 'hand';");

        buttons.add(buttonOk);
        buttons.add(buttonCancel);
    }
}
