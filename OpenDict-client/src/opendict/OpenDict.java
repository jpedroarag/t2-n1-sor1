/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package opendict;

import java.io.IOException;
import java.net.URL;
import java.util.Stack;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import opendict.Alert.AlertType;
import opendict.controllers.FXMLDocumentController;

/**
 * Para amanhã:
 * 
 * PYTHON (servidor):
 * - Banco de palavras
 * 
 */
 


public class OpenDict extends Application {

    public static void setupStage(Parent root, Stage window) {
        Scene scene = new Scene(root);
        scene.getStylesheets().add("https://fonts.googleapis.com/css?family=Charmonman:400,700");
        scene.getStylesheets().add("https://fonts.googleapis.com/css?family=K2D:400,500,600,700");
        
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        window.setX(bounds.getMinX());
        window.setY(bounds.getMinY());
        window.setWidth(bounds.getWidth());
        window.setHeight(bounds.getHeight());
        window.setMaximized(true);
        window.setOnCloseRequest(e -> {
            Alert areYouSure = new Alert("Tem certeza?", "Você realmente deseja sair da aplicação?", AlertType.YES_NO);
            if(!areYouSure.present()) { e.consume(); }
        });
        window.setScene(scene);
    }

    @Override
    public void start(Stage stage) throws IOException {
        URL url = getClass().getResource("controllers/views/FXMLDocument.fxml");
        Parent rootScene = FXMLLoader.load(url);
        OpenDict.setupStage(rootScene, stage);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
